﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereHandler : MonoBehaviour
{
	[SerializeField]
	private float pullRadius;
	[SerializeField]
	private float pullForce;
	[SerializeField]
	private float distanceMultiplier;
	
	public ContactPoint collisionPoint;

	private static int collisionCount;
	public bool Collided = false;

	private Rigidbody sphereRB;

	private MeshRenderer sphereMesh;

	private void Start()
	{
		sphereRB = GetComponent<Rigidbody>();
		sphereMesh = GetComponent<MeshRenderer>();
		ChangeColor();
	}

	void FixedUpdate()
	{
		if (!sphereMesh.isVisible)
		{
			Destroy(this.gameObject);
		}
		foreach (Collider collider in Physics.OverlapSphere(transform.position, pullRadius))
		{
			if (collider.tag == "sphere")
			{
				if (App.Instance.model.gameModel.Count <= App.Instance.controller.game.maxSpheresAmount)
				{
					
					Vector3 direction = transform.position - collider.transform.position;

					var distance = direction.sqrMagnitude * distanceMultiplier + 1;
					var colliderRB = collider.GetComponent<Rigidbody>();
					colliderRB.AddForce(direction.normalized * (pullForce / distance) * colliderRB.mass * Time.fixedDeltaTime);
				}
				else
				{
					Collided = true;
					Vector3 direction = -transform.position + collider.transform.position;

					var distance = direction.sqrMagnitude * distanceMultiplier + 1;
					var colliderRB = collider.GetComponent<Rigidbody>();
					colliderRB.AddForce(direction.normalized * (pullForce / distance) * colliderRB.mass * Time.fixedDeltaTime);
				}
			}
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.collider.tag == "sphere" && !Collided)
		{
			collisionCount++;
			if (collisionCount % 2 == 0)
			{
				Collided = true;
				collisionPoint = other.contacts[0];
				var newBall = Instantiate(App.Instance.model.spherePrefab);
				newBall.GetComponent<SphereCollider>().enabled = false;
				newBall.transform.position = collisionPoint.point;
				var newBallRB = newBall.GetComponent<Rigidbody>();
				newBallRB.mass = sphereRB.mass + other.collider.GetComponent<Rigidbody>().mass;
				newBall.GetComponent<SphereHandler>().pullRadius =
					pullRadius + other.collider.GetComponent<SphereHandler>().pullRadius;
				if (newBallRB.mass > App.Instance.controller.game.maxMassPerSphere)
				{
					for (int i = 0; i <= 50; i++)
					{
						var littleBall = Instantiate(App.Instance.model.spherePrefab);
						littleBall.GetComponent<SphereCollider>().enabled = false;
						littleBall.transform.position = collisionPoint.point;
						littleBall.GetComponent<Rigidbody>().mass = 5;
						var direction = new Vector3(Random.value, Random.value, Random.value);
						littleBall.GetComponent<Rigidbody>().AddForce(direction.normalized * 50, ForceMode.Impulse);
					}
				}
				else
				{
					if (other != null)
					{
						Destroy(other.gameObject);
					}
					Destroy(this.gameObject);
					newBall.GetComponent<SphereCollider>().enabled = true;
				}
			}
		}
	}

	private void ChangeColor()
	{
		var randomColor = Random.ColorHSV();
		sphereMesh.material.color = randomColor;
	}
}
