﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class App : MonoBehaviour
{
    public static App Instance;

    [Serializable]
    public class View
    {
        public Transform plane;
        public Text countText;
    }

    [Serializable]
    public class Model
    {
        public GameObject spherePrefab;
        public GameModel gameModel;
    }

    [Serializable]
    public class Controller
    {
        public SpheresController spheres;
        public GameController game;
    }

    public Model model;
    public View view;
    public Controller controller;


    void Awake()
    {
        App.Instance = this;
    }

}