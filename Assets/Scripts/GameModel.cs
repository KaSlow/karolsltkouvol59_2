﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel : MonoBehaviour
{
    public Action<int> OnCountChange;
    private int count;

    public int Count
    {
        get { return count; }
        set
        {
            count = value;
            if (OnCountChange != null) OnCountChange(count);
        }
    }
}
