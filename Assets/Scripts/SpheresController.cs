﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpheresController : MonoBehaviour
{

	private Transform plane;
	private MeshRenderer planeMesh;
          
	public void Init()
	{
		plane = App.Instance.view.plane;
		planeMesh = plane.GetComponent<MeshRenderer>();
		InvokeRepeating("SpawnSphere", 0, 0.15f);
		App.Instance.model.gameModel.OnCountChange += UpdateView;
	}

	private void SpawnSphere()
	{
		var newBall = Instantiate(App.Instance.model.spherePrefab);
		var x = Random.Range(planeMesh.bounds.min.x, planeMesh.bounds.max.x);
		var z = Random.Range(planeMesh.bounds.min.z, planeMesh.bounds.max.z);
		
		newBall.transform.position = new Vector3(x, Random.Range(1, 5), z);
		App.Instance.model.gameModel.Count++;
	}

	private void UpdateView(int count)
	{
		if (count > 250)
		{
			CancelInvoke("SpawnSphere");
			return;
		}
		App.Instance.view.countText.text = String.Format("spheres amount: {0}", count);
	}
}
